import stomp
from ingest import *


class MsgListener(stomp.ConnectionListener):
    def __init__(self):
        self.msg_recieved = 0

    def on_error(self, message):
        print("Received the error:" + message)

    def on_message(self, message):
        print("Received the message:" + str(message.body))

        data = json.loads(message.body)
        importData(data)


host = [("localhost", 61613)]
conn = stomp.Connection(host_and_ports=host)
conn.set_listener("listener", MsgListener())
conn.connect()
conn.subscribe(destination="/queue/ingestInvestigation", id=1, headers={})

while True:
    pass
