#! /usr/bin/python
from __future__ import print_function
import icat
import icat.config
import json


def hello():
    print("Hello")


def importData(data):
    preset_config = {
        "url": "https://ovm-icat-test:8181",
        "auth": "db",
        "username": "admin",
        "password": "test_admin",
    }

    config = icat.config.Config(ids="optional")
    config.configfile = "icat.cfg"
    client, conf = config.getconfig()

    client.login(conf.auth, conf.credentials)

    print("Login to %s was successful." % conf.url)
    print("User: %s" % client.getUserName())

    # QuerIES
    FACILITY_BY_NAME = "SELECT facility FROM Facility facility where facility.name='{}'"
    USER_BY_NAME = "SELECT user FROM User user where user.name='{}'"
    INVESTIGATION_BY_NAME_AND_VISITID = "SELECT investigation FROM Investigation investigation where investigation.name='{}' and investigation.visitId='{}'"
    INSTRUMENT_BY_NAME = (
        "SELECT instrument FROM Instrument instrument where instrument.name='{}'"
    )
    GROUPING_BY_NAME = "SELECT grouping FROM Grouping grouping where grouping.name='{}'"

    if "users" in data:
        # Adding Users and userGroup
        for user in data["users"]:
            root = client.new("user")
            root.name = user["name"]
            root.fullName = user["fullName"]
            root.email = user["email"] if "email" in user else ""
            root.orcidId = user["orcidId"] if "orcidId" in user else ""
            root.givenName = user["givenName"] if "givenName" in user else ""
            root.familyName = user["familyName"] if "familyName" in user else ""
            client.create(root)

            if "grouping" in user:
                user_group = client.new("userGroup")
                users = client.search(USER_BY_NAME.format(user["name"]))
                user_group.user = users[0]
                groupings = client.search(GROUPING_BY_NAME.format(user["grouping"]))
                user_group.grouping = groupings[0]
                client.create(user_group)

    if "investigations" in data:
        print("Creating investigations")
        for investigation in data["investigations"]:
            print("Creating investigation")
            investigationRecord = client.new("investigation")
            investigationRecord.name = investigation["name"]
            investigationRecord.title = investigation["title"]
            investigationRecord.visitId = investigation["visitId"]
            investigationRecord.summary = investigation["summary"]
            investigationRecord.summary = investigation["summary"]
            investigationRecord.startDate = investigation["startDate"]
            investigationRecord.endDate = investigation["endDate"]
            if "releaseDate" in investigation:
                investigationRecord.releaseDate = investigation["releaseDate"]
            if "doi" in investigation:
                investigationRecord.doi = investigation["doi"]
            investigationRecord.facility = client.search(
                FACILITY_BY_NAME.format("ESRF")
            )[0]
            investigationRecord.type = client.search(
                "SELECT investigationType FROM InvestigationType investigationType where investigationType.name='{}'".format(
                    investigation["investigationType"]
                )
            )[0]
            client.create(investigationRecord)

            investigationRecord = client.search(
                INVESTIGATION_BY_NAME_AND_VISITID.format(
                    investigation["name"], investigation["visitId"]
                )
            )[0]
            print(investigationRecord)

            if "datasets" in investigation:
                for dataset in investigation["datasets"]:
                    if "sample" in dataset:
                        sampleRecord = client.new("sample")
                        sampleRecord.name = dataset["sample"]["name"]
                        sampleRecord.investigation = investigationRecord
                        sampleId = client.create(sampleRecord)
                        if "parameters" in dataset["sample"]:
                            sampleRecord = client.search(
                                "SELECT sample from Sample sample where sample.id={}".format(
                                    sampleId
                                )
                            )[0]
                            for parameter in dataset["sample"]["parameters"]:
                                sampleParameterRecord = client.new("sampleParameter")
                                sampleParameterRecord.sample = sampleRecord
                                sampleParameterRecord.type = client.search(
                                    "SELECT parameter from ParameterType parameter where parameter.name='{}'".format(
                                        parameter["name"]
                                    )
                                )[0]
                                sampleParameterRecord.stringValue = parameter[
                                    "stringValue"
                                ]
                                client.create(sampleParameterRecord)
                    else:
                        sampleId = None

                    datasetRecord = client.new("dataset")
                    datasetRecord.name = dataset["name"]
                    if "location" in dataset:
                        datasetRecord.location = dataset["location"]
                    if sampleId is not None:
                        datasetRecord.sample = client.search(
                            "SELECT sample from Sample sample where sample.name='{}'".format(
                                dataset["sample"]["name"]
                            )
                        )[0]
                    datasetRecord.investigation = investigationRecord
                    datasetRecord.type = client.search(
                        "SELECT datasetType from DatasetType datasetType where datasetType.name='{}'".format(
                            dataset["type"]
                        )
                    )[
                        0
                    ]  # TODO: check python icat, it's not mandatory https://repo.icatproject.org/site/icat/server/4.9.0/schema.html#Dataset

                    datasetId = client.create(datasetRecord)

                    datasetRecord = client.search(
                        "SELECT dataset from Dataset dataset where dataset.id={}".format(
                            datasetId
                        )
                    )[0]
                    if "datafiles" in dataset:
                        for datafile in dataset["datafiles"]:
                            datafileRecord = client.new("datafile")
                            datafileRecord.name = datafile["name"]
                            datafileRecord.location = datafile["location"]
                            datafileRecord.dataset = datasetRecord
                            client.create(datafileRecord)

                    if "parameters" in dataset:
                        for parameter in dataset["parameters"]:
                            datasetParameterRecord = client.new("datasetParameter")
                            datasetParameterRecord.dataset = datasetRecord
                            datasetParameterRecord.type = client.search(
                                "SELECT parameter from ParameterType parameter where parameter.name='{}'".format(
                                    parameter["name"]
                                )
                            )[0]
                            datasetParameterRecord.stringValue = parameter[
                                "stringValue"
                            ]
                            client.create(datasetParameterRecord)
                    if "dataCollections" in dataset:
                        for dataCollection in dataset["dataCollections"]:
                            datasetCollectionRecord = client.new("dataCollection")
                            datasetCollectionRecord.doi = dataCollection["doi"]
                            dataCollectionId = client.create(datasetCollectionRecord)
                            datasetCollectionRecord = client.search(
                                "SELECT dataCollection from DataCollection dataCollection where dataCollection.id={}".format(
                                    dataCollectionId
                                )
                            )[0]
                            datasetCollectionDatasetRecord = client.new(
                                "dataCollectionDataset"
                            )
                            datasetCollectionDatasetRecord.dataset = datasetRecord
                            datasetCollectionDatasetRecord.dataCollection = (
                                datasetCollectionRecord
                            )
                            client.create(datasetCollectionDatasetRecord)

            if "investigationInstruments" in investigation:
                for investigationInstruments in investigation[
                    "investigationInstruments"
                ]:
                    investigationInstrumentsRecord = client.new(
                        "investigationInstrument"
                    )
                    investigationInstrumentsRecord.investigation = investigationRecord
                    investigationInstrumentsRecord.instrument = client.search(
                        INSTRUMENT_BY_NAME.format(investigationInstruments["name"])
                    )[0]
                    client.create(investigationInstrumentsRecord)

            if "investigationUsers" in investigation:
                for investigationUser in investigation["investigationUsers"]:
                    investigationUserRecord = client.new("investigationUser")
                    investigationUserRecord.role = investigationUser["role"]
                    investigationUserRecord.user = client.search(
                        USER_BY_NAME.format(investigationUser["name"])
                    )[0]
                    investigationUserRecord.investigation = investigationRecord
                    client.create(investigationUserRecord)

    client.logout()
    print("Ingest finished and logged out")
