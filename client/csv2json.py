import csv
import json
import itertools

csv_file_path = "SEMESTER_DATA.csv"
json_file_path = "SEMESTER_DATA.json"


def toInvestigation(
    name, title, summary, visitId, startDate, endDate, investigationType, users
):
    return {
        "name": name,
        "title": title,
        "summary": summary,
        "visitId": visitId,
        "startDate": startDate,
        "endDate": endDate,
        "investigationType": investigationType,
        "facility": "SESAME",
        "investigationUsers": users,
        "investigationInstruments": [{"name": "BM08 - XAFS/XRF (X-ray Absorption Fine Structure/X-ray Fluorescence) spectroscopy beamline"}],
    }


# Read CSV file and convert it to a dictionary
with open(csv_file_path, mode="r") as csv_file:
    csv_reader = csv.DictReader(csv_file)
    data = [row for row in csv_reader]

# Write the dictionary to a JSON file
with open(json_file_path, mode="w") as json_file:
    json.dump(data, json_file)


def getUsers(record):
    users = []
    for user in record["people"]:
        users.append(
            {
                "name": user["USER_ID"],
                "fullName": user["LASTNAME"],
                "role": "participant",
            }
        )
    return users


with open(json_file_path) as json_file:
    data = json.load(json_file)

    groupingBy = "PROPOSAL_ID"
    # Group the data by PROPOSAL_ID
    grouped_data = []
    for key, group in itertools.groupby(data, lambda x: x[groupingBy]):
        group_dict = {groupingBy: key, "people": list(group)}
        grouped_data.append(group_dict)

    # Convert the grouped data to JSON format
    json_grouped_data = json.dumps(grouped_data, indent=4)

    investigations = []
    for record in grouped_data:
        inv = record["people"][0]
        investigations.append(
            toInvestigation(
                inv["PROPOSAL_ID"],
                inv["TITLE"],
                inv["TITLE"],
                inv["TITLE"],
                inv["SUBMISSION_DATE"],
                inv["SUBMISSION_DATE"],
                
                "SUP",
		getUsers(record),
            )
        )

    print(json.dumps({ "investigations" : investigations}))
