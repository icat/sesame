import json
import sys
import os, shutil
from pprint import pprint
import configparser
import argparse
from MessagingClient import StompClient
from datetime import datetime
from datetime import date
import time
from dataset import dataset, parameter, sample, datafile

##########################
# INPUT PARAMETERS
##########################
parser = argparse.ArgumentParser()
parser.add_argument(
    "--queue",
    help="ActiveMQ queue URLS. Example: bcu-mq-01.esrf.fr:61613",
    required=True,
)
parser.add_argument(
    "--file",
    help="JSON path with the investigations",
    default="data.json",
    required=False,
)
args = parser.parse_args()


#########################
# ACTIVEMQ CALL
########################
client = StompClient([args.queue], "/queue/ingestInvestigation", "")
client.connect()


f = open(args.file)
client.sendMessage(str(f.read()))
