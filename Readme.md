# Documentation

## Configuration

### ActiveMQ

This project assumes that there is an activeMQ instance running. This can be done with docker:

```
docker run --name='activemq' -d \
-e 'ACTIVEMQ_ADMIN_LOGIN=admin' -e 'ACTIVEMQ_ADMIN_PASSWORD=your_password' \
-e 'ACTIVEMQ_CONFIG_MINMEMORY=1024' -e  'ACTIVEMQ_CONFIG_MAXMEMORY=4096' \
-v /tmp/activemq:/data \
-v /tmp/log/activemq:/var/log/activemq \
-p 8161:8161 \
-p 61616:61616 \
-p 61613:61613 \
webcenter/activemq:5.14.3
```

### python-icat

The configuration is made in the file `./client/icat.cfg`. Example:

```
[icat]
url = https://ovm-icat-test:8181
auth = db
username = admin
password = *******

```

## Run

### Parsing CSV file

```
python csv2json.py > investigations.json
```

### Start up the consumer

```
python consumer.py -c icat.cfg -s icat --no-check-certificate
```

### Send investigations for ingestion

```
cd client
python ingestInvestigation.py --queue localhost:61613 --file investigations.json

```
